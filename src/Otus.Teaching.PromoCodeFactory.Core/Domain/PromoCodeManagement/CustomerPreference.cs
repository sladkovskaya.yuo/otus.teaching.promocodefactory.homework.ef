﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference : BaseEntity
    {
        public Guid CustomerId;

        public Guid PreferenceId;

        public virtual Customer Customer { get; set; }

        public virtual Preference Preference { get; set; }
    }
}
