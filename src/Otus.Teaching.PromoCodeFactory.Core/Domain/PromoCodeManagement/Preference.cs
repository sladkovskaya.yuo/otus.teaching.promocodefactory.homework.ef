﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(13)]
        public string ForMigration { get; set; }

        public virtual ICollection<CustomerPreference> Customers { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }

    }
}