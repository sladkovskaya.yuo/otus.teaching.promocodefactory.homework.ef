﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        [HttpGet]
        public async Task<ActionResult<PreferenceResponce>> GetPreferenceAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferencesResponce = preferences.Select(_ => new PreferenceResponce()
            {
                Id = _.Id,
                Name = _.Name
            });
            return Ok(preferencesResponce);
        }
    }

}
