﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository, IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = customers
                .Select(_ => new CustomerShortResponse()
                {
                    Id = _.Id,
                    Email = _.Email,
                    FirstName = _.FirstName,
                    LastName = _.LastName,
                })
                .ToList();
            return Ok(response);
        }

        /// <summary>
        /// Получение клиента с подробной информацией по ID.
        /// </summary>
        /// <param name="id">Идентификатор клиекта</param>
        /// <returns>Информация о клиенте.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var preferences = customer?.Preferences
                .Select(_ => new PreferenceResponce()
                {
                    Id = _.PreferenceId,
                    Name = _.Preference.Name
                })
                .ToList();
            var promoCodes = customer?.PromoCodes
                .Select(_ => new PromoCodeShortResponse()
                {
                    Code = _.Code,
                    Id = _.Id,
                    BeginDate = _.BeginDate.ToLongDateString(),
                    EndDate = _.EndDate.ToLongDateString(),
                    ServiceInfo = _.ServiceInfo,
                    PartnerName = _.PartnerName,
                }).
                ToList();
            var responce = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = preferences,
                PromoCodes = promoCodes
            };
            return Ok(responce);
        }


        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                Email = request.Email,
                LastName = request.LastName
            };
            customer.Preferences = CreateCustomerPreferences(request, await _preferenceRepository.GetAllAsync(), customer);
            await _customerRepository.CreateAsync(customer);
            return Ok();
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            customer.FirstName = request.FirstName;
            customer.Email = request.Email;
            customer.LastName = request.LastName;
            customer.Preferences = CreateCustomerPreferences(request, await _preferenceRepository.GetAllAsync(), customer);

            await _customerRepository.UpdateAsync(customer);
            return Ok();
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            while (customer.PromoCodes?.Count() > 0)
            {
                var promoCode = customer.PromoCodes.First();
                await _promoCodeRepository.DeleteAsync(promoCode.Id);
                customer.PromoCodes.Remove(promoCode);
            }
            foreach (var preference in customer.Preferences)
            {
                await _preferenceRepository.DeleteAsync(preference.Id);
            } 
            await _customerRepository.DeleteAsync(id);

            return Ok();

        }

        private static List<CustomerPreference> CreateCustomerPreferences(CreateOrEditCustomerRequest request, IEnumerable<Preference> _preferenceRepository, Customer customer)
        {
            var preferences = new List<CustomerPreference>();
            foreach (var id in request.PreferenceIds)
            {
                preferences.Add(
                    new CustomerPreference()
                    {
                        Id = new Guid(),
                        Preference = _preferenceRepository.FirstOrDefault(x => x.Id == id) ?? throw new Exception($"No Preference with id {id}"),
                        Customer = customer
                    });
            };
            return preferences;
        }
}
}